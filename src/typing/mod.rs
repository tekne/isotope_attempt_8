/*!
The isotope type system
*/

use std::num::NonZeroUsize;

use crate::value::Value;

pub mod function;
use function::FunctionType;

/// A marker trait for typing contexts
pub trait Context {
    //TODO
}

/// A dynamic typing context, i.e., one which carries information.
pub trait DynContext : Context {
    //TODO
}

/// The static typing context ZST
pub struct StaticContext;
impl Context for StaticContext {
    //TODO
}
impl<C: DynContext> From<C> for StaticContext { fn from(_ : C) -> StaticContext { StaticContext } }

/// A typing universe
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct Universe(NonZeroUsize);

impl Universe {
    /// The universe of simple values
    #[inline(always)]
    pub fn set() -> Universe { Universe(NonZeroUsize::new(1).unwrap()) }
    /// The universe containing this universe
    #[inline(always)]
    pub fn next(self) -> Universe { Universe(NonZeroUsize::new(self.0.get() + 1).unwrap()) }
}

/// A type which can be interpreted in a fixed typing context
pub trait ConType: Eq {
    /// The typing context necessary to interpret this type
    type Context : Context;
    /// This type kind's function subtype, if any
    type ConAsFunc : FunctionType<Self::Context>;

    /// The typing level of this type
    fn level_c(&self, ctx : Self::Context) -> Universe;
    /// Whether this type (always) holds types
    fn is_kind_c(&self, ctx : Self::Context) -> bool;
    /// Whether this type is a function. If so, try_as_func_c must succeed
    /// assuming the value is not modified
    fn is_func_c(&self, ctx : Self::Context) -> bool;
    /// Attempt to convert this type to a function type. On failure, return
    /// the unit tuple
    fn try_as_func_c(self, ctx : Self::Context) -> Result<Self::ConAsFunc, ()>;

    //TODO: rest
}

impl ConType for Universe {
    type Context = StaticContext;
    type ConAsFunc = !;
    #[inline(always)] fn level_c(&self, _ctx : Self::Context) -> Universe { self.next() }
    #[inline(always)] fn is_kind_c(&self, _ctx : Self::Context) -> bool { true }
    #[inline(always)] fn is_func_c(&self, _ctx : Self::Context) -> bool { false }
    #[inline(always)] fn try_as_func_c(self, _ctx : Self::Context) -> Result<!, ()> { Err(()) }
}

/// A type with no higher structure, which is never a function
pub trait SimpleType: Eq {}

impl<T: SimpleType> ConType for T {
    type Context = StaticContext;
    type ConAsFunc = !;

    #[inline(always)] fn level_c(&self, _ctx : Self::Context) -> Universe { Universe::set() }
    #[inline(always)] fn is_kind_c(&self, _ctx : Self::Context) -> bool { false }
    #[inline(always)] fn is_func_c(&self, _ctx : Self::Context) -> bool { false }
    #[inline(always)] fn try_as_func_c(self, _ctx : Self::Context) -> Result<Self::ConAsFunc, ()> {
        Err(()) }
}

// The unit type is a simple type
impl SimpleType for () {}

/// The empty type, which has no values
#[derive(Debug, Eq, PartialEq, Hash, Copy, Clone)]
pub struct Empty;
impl SimpleType for Empty {}

/// A type which can be interpreted in a given typing context. You usually
/// want to implement `ConType`, which implements this automatically for
/// compatible contexts
pub trait Type<C: Context>: Eq {
    /// This type kind's function subtype, if any
    type AsFunc : FunctionType<C>;

    /// The typing level of this type
    fn level(&self, ctx : C) -> Universe;
    /// Whether this type (always) holds types
    fn is_kind(&self, ctx : C) -> bool;
    /// Whether this type is a function. If so, try_as_func_c must succeed
    /// assuming the type is not modified
    fn is_func(&self, ctx : C) -> bool;
    /// Attempt to convert this type to a function type. On failure, return
    /// the unit tuple
    fn try_as_func(self, ctx : C) -> Result<Self::AsFunc, ()>;
}

impl<T, C> Type<C> for T where
    T: ConType, T::ConAsFunc: FunctionType<C>, C: Context + Into<T::Context> {
    type AsFunc = T::ConAsFunc;
    /// The typing level of this type
    #[inline] fn level(&self, ctx : C) -> Universe { self.level_c(ctx.into()) }
    /// Whether this type (always) holds types
    #[inline] fn is_kind(&self, ctx : C) -> bool { self.is_kind_c(ctx.into()) }
    /// Whether this type is a function. If so, try_as_func_c must succeed
    /// assuming the type is not modified
    #[inline] fn is_func(&self, ctx : C) -> bool { self.is_func_c(ctx.into()) }
    /// Attempt to convert this type to a function type. On failure, return
    /// the unit tuple
    #[inline] fn try_as_func(self, ctx : C) -> Result<Self::AsFunc, ()> {
        self.try_as_func_c(ctx.into())
    }
}

impl<C: Context> Type<C> for ! {
    type AsFunc = !;
    fn level(&self, _ctx : C) -> Universe { *self }
    fn is_kind(&self, _ctx : C) -> bool { *self }
    fn is_func(&self, _ctx : C) -> bool { *self }
    fn try_as_func(self, _ctx : C) -> Result<Self::AsFunc, ()> { self }
}

/// Interpret a type as a value
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct AsValue<T>(pub T);

impl<T, C, K> Value<C, K> for AsValue<T> where
    T: Type<C> + Clone,
    C: Context,
    K: Type<C>,
    Universe: Into<K> {
    type AsType = T;
    fn get_type(&self, ctx : C) -> K { self.0.level(ctx).into() }
    fn is_type(&self, _ctx : C) -> bool { true }
    fn try_as_type(&self, _ctx : C) -> Result<Self::AsType, ()> { Ok(self.0.clone()) }
}
