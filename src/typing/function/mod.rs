/*!
The trait for (potentially dependent) function types, the fundamental isotope type former.
*/

use derive_more::From;
use std::num::NonZeroUsize;

use crate::value::Value;
use super::{Type, Context};

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct FunctionTypeMismatch { pub pos : usize }

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct TooFewArgs { pub how_few : NonZeroUsize }

impl TooFewArgs {
    pub fn new(amount : usize) -> Option<TooFewArgs> {
        NonZeroUsize::new(amount).map(|nz| TooFewArgs { how_few : nz })
    }
    pub fn one() -> Self {
        Self::new(1).unwrap()
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
#[derive(From)]
pub enum FunctionTypeError {
    FunctionTypeMismatch(FunctionTypeMismatch),
    TooFewArgs(TooFewArgs)
}

/// A type which can simply be interpreted as a (single-argument) function in a given
/// typing context. Automatically implements the multi-argument optimized
/// `FunctionType`
pub trait SingleFunctionType<C: Context> : Type<C> {
    /// The type of arguments to this function
    type SingleArgType: Type<C>;
    /// The result type of this function for a single application
    type SingleResType: Type<C>;
    /// The result type of applying this function to a given argument
    fn apply_s<V, CV>(&self, val : &V, ctx : CV)
        -> Result<Self::SingleResType, FunctionTypeMismatch> where
        CV: Context + Into<C>, Self::SingleArgType: Type<CV>, V: Value<CV, Self::SingleArgType>;
}

impl<C: Context> SingleFunctionType<C> for ! {
    type SingleArgType = !;
    type SingleResType = !;
    fn apply_s<V, CV>(&self, _val : &V, _ctx : CV)
        -> Result<Self::SingleResType, FunctionTypeMismatch> { *self }
}

/// A type which can be interpreted as a function in a given typing context.
pub trait FunctionType<C: Context> : Type<C> {
    /// The type of arguments to this function
    type ArgType: Type<C>;
    /// The result type of this function for a single application
    type ResType: Type<C>;
    /// An efficient representation for *below* args_no applications.
    type PartialResType: Type<C>;
    /// An efficient representation for *exactly* args_no applications.
    /// Any more applications may require a separate type, if even possible.
    type TotalResType: Type<C>;
    /// An efficient representation for up to args_no applications, and maybe more.
    type MultiResType: Type<C>;
    /// A lower bound on the number of arguments this function supports
    fn args_no(&self) -> usize;
    /// The result type of applying this function to a given argument
    fn apply<V, CV>(&self, val: &V, ctx: CV) -> Result<Self::ResType, FunctionTypeMismatch> where
        CV: Context + Into<C>, Self::ArgType: Type<CV>, V: Value<CV, Self::ArgType>;
    /// The result type of applying this function to an iterator of arguments,
    /// as far as it will go efficiently, but always less than args_no.
    /// At least one argument should be provided.
    fn partially_applied<'a, VS, V, CV>(&'a self, val: &mut VS, ctx: CV)
    -> Result<Self::PartialResType, FunctionTypeError> where
        C: Clone,
        CV: Context + Into<C> + Clone,
        Self::ArgType: Type<CV>,
        VS: Iterator<Item=&'a V>,
        V: 'a + Value<CV, Self::ArgType>;
    /// The result type of applying this function to an iterator of arguments,
    /// as far as it will go efficiently, *asserting* at least args_no arguments will
    /// be provided. More *may* be consumed.
    /// At least one argument should be provided.
    fn totally_applied<'a, VS, V, CV>(&'a self, val: &mut VS, ctx: CV)
    -> Result<Self::TotalResType, FunctionTypeError> where
        C: Clone,
        CV: Context + Into<C> + Clone,
        Self::ArgType: Type<CV>,
        VS: Iterator<Item=&'a V>,
        V: 'a + Value<CV, Self::ArgType>;
    /// The result type of applying this function to an iterator of arguments,
    /// as far as it will go efficiently, with no constraint on how many arguments are
    /// passed in. At least one argument should be provided.
    fn applied<'a, VS, V, CV>(&'a self, val: &mut VS, ctx: CV)
    -> Result<Self::MultiResType, FunctionTypeError> where
        C: Clone,
        CV: Context + Into<C> + Clone,
        Self::ArgType: Type<CV>,
        VS: Iterator<Item=&'a V>,
        V: 'a + Value<CV, Self::ArgType>;
}

impl<C, T> FunctionType<C> for T where C: Context, T: SingleFunctionType<C> {
    type ArgType = T::SingleArgType;
    type ResType = T::SingleResType;
    type PartialResType = !;
    type TotalResType = T::SingleResType;
    type MultiResType = T::SingleResType;
    #[inline(always)] fn args_no(&self) -> usize { 1 }
    #[inline(always)]
    fn apply<V, CV>(&self, val : &V, ctx : CV) -> Result<Self::ResType, FunctionTypeMismatch>
        where CV: Context + Into<C>, Self::ArgType: Type<CV>, V: Value<CV, Self::ArgType> {
        self.apply_s(val, ctx)
    }
    #[inline]
    fn partially_applied<'a, VS, V, CV>(&'a self, _val: &mut VS, _ctx: CV)
    -> Result<Self::PartialResType, FunctionTypeError> where
        CV: Context + Into<C>,
        Self::ArgType: Type<CV>,
        VS: Iterator<Item=&'a V>,
        V: 'a + Value<CV, Self::ArgType> {
        Err(TooFewArgs::one().into())
    }
    #[inline]
    fn totally_applied<'a, VS, V, CV>(&'a self, val: &mut VS, ctx: CV)
    -> Result<Self::TotalResType, FunctionTypeError> where
        CV: Context + Into<C>,
        Self::ArgType: Type<CV>,
        VS: Iterator<Item=&'a V>,
        V: 'a + Value<CV, Self::ArgType> {
        match val.next() {
            Some(arg) => self.apply_s(arg, ctx).map_err(|e| e.into()),
            None => Err(TooFewArgs::one().into())
        }
    }
    #[inline(always)]
    fn applied<'a, VS, V, CV>(&'a self, val: &mut VS, ctx: CV)
    -> Result<Self::MultiResType, FunctionTypeError> where
        CV: Context + Into<C>,
        Self::ArgType: Type<CV>,
        VS: Iterator<Item=&'a V>,
        V: 'a + Value<CV, Self::ArgType> {
        match val.next() {
            Some(arg) => self.apply_s(arg, ctx).map_err(|e| e.into()),
            None => Err(TooFewArgs::one().into())
        }
    }
}
