/*!
The isotope intermediate language
*/

#![feature(never_type)]

pub mod typing;
pub mod value;
pub mod block;
