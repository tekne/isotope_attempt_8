/*!
Isotope values
*/

use crate::typing::{Type, Context, StaticContext};

/// A value which can be interpreted in a fixed context
pub trait ConValue {
    /// The typing context needed to interpret this value
    type Context : Context;
    /// The kind of types this value can have
    type TypeKind : Type<Self::Context>;
    /// This value itself taken as a type, if possible
    type ConAsType : Type<Self::Context>;

    /// Get the type of this value
    fn get_type_c(&self, ctx : Self::Context) -> Self::TypeKind;
    /// Whether this value is a type. If so, try_as_type_c must succeed
    /// assuming the value is not modified
    fn is_type_c(&self, ctx : Self::Context) -> bool;
    /// Attempt to convert this value to a type. On failure, return
    /// the unit tuple
    fn try_as_type_c(&self, ctx : Self::Context) -> Result<Self::ConAsType, ()>;
}

// The unit element of the unit type
impl ConValue for () {
    type Context = StaticContext;
    type TypeKind = ();
    type ConAsType = !;
    #[inline(always)] fn get_type_c(&self, _ctx : Self::Context) -> () { () }
    #[inline(always)] fn is_type_c(&self, _ctx : Self::Context) -> bool { false }
    #[inline(always)] fn try_as_type_c(&self, _ctx : Self::Context) -> Result<!, ()> { Err(()) }
}

/// A value which can be interpreted in a given typing context
pub trait Value<C, K> where C: Context, K: Type<C> {
    type AsType : Type<C>;

    /// Get the type of this value
    fn get_type(&self, ctx : C) -> K;
    /// Whether this value is a type. If so, try_as_type_c must succeed
    /// assuming the value is not modified
    fn is_type(&self, ctx : C) -> bool;
    /// Attempt to convert this value to a type. On failure, return
    /// the unit tuple
    fn try_as_type(&self, ctx : C) -> Result<Self::AsType, ()>;
}

impl<V, C, K> Value<C, K> for V where
    V: ConValue,
    C: Context + Into<V::Context>,
    K: Type<C>,
    V::TypeKind: Into<K>,
    V::ConAsType: Type<C> {
    type AsType = V::ConAsType;
    #[inline] fn get_type(&self, ctx : C) -> K { self.get_type_c(ctx.into()).into() }
    #[inline] fn is_type(&self, ctx : C) -> bool { self.is_type_c(ctx.into()) }
    #[inline] fn try_as_type(&self, ctx : C) -> Result<Self::AsType, ()> {
        self.try_as_type_c(ctx.into()) }
}

impl<C: Context, K: Type<C>> Value<C, K> for ! {
    type AsType = !;
    fn get_type(&self, _ctx : C) -> K { *self }
    fn is_type(&self, _ctx : C) -> bool { *self }
    fn try_as_type(&self, _ctx : C) -> Result<Self::AsType, ()> { *self }
}
