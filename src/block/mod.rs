/*!
Basic blocks in an isotope CFG, which are used to define functions via the
correspondence between SSA form and functional programming
*/
use typed_generational_arena::{StandardArena as Arena, StandardIndex as Index};
use derive_more::{Index, IndexMut};

use crate::typing::{Type, Context, DynContext};
use crate::value::Value;

/// A graph-like typing context, for easily representing complex type structures
#[derive(Index, IndexMut)]
pub struct GraphContext<V> {
    pub values : Arena<V>
}

impl<'a, V> Context for &'a GraphContext<V> {}
impl<'a, V> DynContext for &'a GraphContext<V> {}

impl<'a, V, K> Value<&'a GraphContext<V>, K> for Index<V> where
    K: Type<&'a GraphContext<V>>,
    V: Value<&'a GraphContext<V>, K> {
    type AsType = V::AsType;

    fn get_type(&self, ctx: &'a GraphContext<V>) -> K { ctx[*self].get_type(ctx) }
    fn is_type(&self, ctx: &'a GraphContext<V>) -> bool { ctx[*self].is_type(ctx) }
    fn try_as_type(&self, ctx: &'a GraphContext<V>) -> Result<V::AsType, ()> {
        ctx[*self].try_as_type(ctx)
    }
}
